export interface CKEditorConfig {
  language: string;
  extraPlugins: string;
  filebrowserUploadUrl: string;
  filebrowserBrowseUrl: string;
  height: number;
  extraAllowedContent: string;
  codeSnippet_theme: string;
}

const config: CKEditorConfig = {
  language            : 'en',
  extraPlugins        : 'uploadimage,codesnippet',
  codeSnippet_theme   : 'github',
  filebrowserUploadUrl: `${process.env.VUE_APP_API_URL}/ckeditor-upload`,
  filebrowserBrowseUrl: '/file-browser?type=Files',
  height              : 500,
  extraAllowedContent : 
  '*[*]{*}'
};

export default config;