const apiURL = process.env.VUE_APP_API_URL;

const getFullUploadPath = (path: string): string => `${apiURL}${path}`;

export default getFullUploadPath;