interface Message {
  title: string;
  message: string;
  type?: MessageType;
}
type MessageType = 'success' | 'warning' | 'error' | 'info';

export const successfullUpdatedMessage: Message = {
  title  : 'Saved',
  message: 'Succesfully updated',
  type   : 'success'
};

export const successfullRemovedMessage: Message = {
  title  : 'Saved',
  message: 'Succesfully removed',
  type   : 'success'
};

export const successfullCreatedMessage: Message = {
  title  : 'Saved',
  message: 'Succesfully created',
  type   : 'success'
};

export const errorRemovedMessage: Message = {
  title  : 'Error',
  message: 'Removing was failed',
  type   : 'error'
};

export const errorCreatedMessage: Message = {
  title  : 'Error',
  message: 'Creating was failed',
  type   : 'error'
};

export const errorUpdatedMessage: Message = {
  title  : 'Error',
  message: 'Updating was failed',
  type   : 'error'
};

export const authErrorMessage: Message = {
  title  : 'Error',
  message: 'Authorization was failed',
  type   : 'error'
};