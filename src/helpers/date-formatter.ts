const dateFormatter = (date: string): string => {
  const dateObj = new Date(date);
  let dd = dateObj.getDate().toString();
  let mm = (dateObj.getMonth() + 1).toString();
  const yyyy = dateObj.getFullYear();

  if (Number(dd) < 10) {
    dd = '0' + dd;
  }
  if (Number(mm) < 10) {
    mm = '0' + mm;
  }

  let hours = dateObj.getHours().toString();
  hours = hours.length === 1 ? `0${hours}` : hours;
  
  let minutes = dateObj.getMinutes().toString();
  minutes = minutes.length === 1 ? `0${minutes}` : minutes;

  return `${dd}.${mm}.${yyyy}, ${hours}:${minutes}`;
};

export default dateFormatter;