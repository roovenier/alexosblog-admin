import { Module, VuexModule, Mutation } from 'vuex-module-decorators';

@Module({
  name        : 'Preloader',
  namespaced  : true,
  stateFactory: true,
})
export default class Preloader extends VuexModule {
  isPreloaderActive = false

  @Mutation
  SET_PRELOADER_STATE(value: boolean): void {
    this.isPreloaderActive = value;
  }
}