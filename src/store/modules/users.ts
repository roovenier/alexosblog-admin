import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { UserItem }                             from '~/api/users/types';
import api                                      from '~/utils/api';

interface EditPaylod {
  firstName?: string;
  lastName?: string;
  email?: string;
  isActive?: boolean;
}

@Module({
  name        : 'Users',
  namespaced  : true,
  stateFactory: true,
})
export default class Users extends VuexModule {
  userList: Array<UserItem> = []
  editDialogVisible = false
  userForEdit: UserItem | null = null;

  @Mutation
  SET_USERS(users: Array<UserItem>): void {
    this.userList = users;
  }

  @Mutation
  SET_DIALOG_VISIBLE(value: boolean): void {
    this.editDialogVisible = value;
  }

  @Mutation
  SET_USER_FOR_EDIT({userId, values = {}}: {userId: number; values?: EditPaylod}): void {
    const user = this.userList.find((user) => user.id === userId);
    this.userForEdit = user ? {...user, ...values} : null;
  }

  @Action({ commit : 'SET_USERS' })
  async getUsers(): Promise<Array<UserItem>> {
    const result = await api.users.getUsers();
    return result.data.users;
  }

  @Action
  async updateUser(): Promise<void> {
    if(this.userForEdit) {
      const { id, email, isActive, firstName, lastName } = this.userForEdit;
      await api.users.updateUser({ id, email, isActive, firstName, lastName });
    }
  }
}