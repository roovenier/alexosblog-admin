import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { Post }                                 from '~/api/posts/types';
import { TagItem }                              from '~/api/tags/types';
import { LanguageItem }                         from '~/api/languages/types';
import api                                      from '~/utils/api';

export interface CreatePostForm {
  title: string;
  tags: Array<number>;
}

const createPostDefault: CreatePostForm = {
  title: '',
  tags : []
};

export interface DetailPost extends Post {
  tags: Array<number>;
}

const detailPostDefault: DetailPost = {
  id       : 0,
  userId   : 0,
  title    : '',
  postItems: [],
  tags     : []
};

@Module({
  name      : 'Posts',
  namespaced: true
})
export default class Posts extends VuexModule {
    postList: Array<Post> = []
    postCreateForm: CreatePostForm = {...createPostDefault}
    postEditForm: DetailPost = {...detailPostDefault}
    postDetail: DetailPost = {...detailPostDefault}

    get getAvailableLangsForCreate(): Array<LanguageItem> {
      const langList: Array<LanguageItem> = this.context.rootState.Languages.langList;
      const detailPostLangs = this.postDetail.postItems.map((postItem) => postItem.lang.id);
      return langList.filter((lang) => !detailPostLangs.includes(lang.id));
    }

    get getAvailableLangsForEdit(): Array<LanguageItem> {
      const langList: Array<LanguageItem> = this.context.rootState.Languages.langList;
      const editPostItem = this.context.rootState.PostItems.postItemEditForm;
      const detailPostLangs = this.postDetail.postItems.map((postItem) => postItem.lang.id).filter((id) => id !== editPostItem.lang);
      return langList.filter((lang) => !detailPostLangs.includes(lang.id));
    }

    get getPostList(): Array<Post> {
      return this.postList.map((post) => {
        const postItems = post.postItems.map((postItem) => ({...postItem, lang : {...postItem.lang, logo : postItem.lang.logo ? this.context.rootState.apiURL + postItem.lang.logo : ''}}));
        return {...post, postItems};
      });
    }

    get getPostEditForm(): DetailPost {
      const postItems = this.postEditForm.postItems.map((postItem) => ({...postItem, lang : {...postItem.lang, logo : postItem.lang.logo ? this.context.rootState.apiURL + postItem.lang.logo : ''}}));
      return {...this.postEditForm, postItems};
    }

    @Mutation
    SET_POST_LIST(list: Array<Post>): void {
      this.postList = list;
    }

    @Mutation
    SET_DETAIL_POST(post: Post): void {
      const tags = (post.tags as Array<TagItem>).map((tag) => tag.id);
      this.postDetail = {...post, tags};
      this.postEditForm = {...post, tags};
    }

    @Mutation
    RESET_CREATE_FORM(): void {
      this.postCreateForm = {...createPostDefault};
    }

    @Action({ commit : 'SET_DETAIL_POST' })
    async getDetailPost(postId: number): Promise<Post> {
      const result = await api.posts.getPostById(postId);
      return result.data.postByIdAdmin;
    }

    @Action({ commit : 'SET_POST_LIST' })
    async getPosts(): Promise<Array<Post>> {
      const result = await api.posts.getUserPosts(this.context.rootGetters['Auth/getUser'].id);
      return result.data.postsByUser;
    }

    @Action({rawError : true})
    async createPost(): Promise<void> {
      const payload = {
        title : this.postCreateForm.title,
        tagIds: this.postCreateForm.tags
      };
      await api.posts.createPost(payload);
    }

    @Action
    async updatePost(): Promise<void> {
      const payload = {
        id    : this.postEditForm.id,
        title : this.postEditForm.title,
        tagIds: this.postEditForm.tags
      };

      await api.posts.updatePost(payload);
    }

    @Action
    async removePost(postId: number): Promise<void> {
      await api.posts.removePost(postId);
    }
}