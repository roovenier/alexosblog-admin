import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import api                                      from '~/utils/api';
import { LanguageItem }                         from '~/api/languages/types';
import { ImageFile }                            from '~/store/types';
import getFullUploadPath                        from '~/helpers/upload-path';

export interface CreateLangForm {
  name: string;
  logoFiles: Array<ImageFile>;
}

interface LangEditItem extends LanguageItem {
  logoFiles: Array<ImageFile>;
}

const createLangFormDefault = {
  name     : '',
  logoFiles: []
};

type FormType = 'createLangForm' | 'langForEdit';

@Module({
  name        : 'Languages',
  namespaced  : true,
  stateFactory: true,
})
export default class Languages extends VuexModule {
  langList: Array<LanguageItem> = [];
  editDialogVisible = false;
  langForEdit: LangEditItem | null = null;
  createLangForm: CreateLangForm = {...createLangFormDefault};

  @Mutation
  SET_LANGUAGE_LIST(list: Array<LanguageItem>): void {
    this.langList = list;
  }

  @Mutation
  SET_DIALOG_VISIBLE(value: boolean): void {
    this.editDialogVisible = value;
  }

  @Mutation
  SET_LANGUAGE_FOR_EDIT(lang: LanguageItem): void {
    const langForEdit = {
      ...lang,
      logoFiles: lang.logo ? [{name : lang.logo, response : lang.logo, url : getFullUploadPath(lang.logo)}] : []
    };
    this.langForEdit = langForEdit;
  }

  @Mutation
  RESET_CREATE_LANG(): void {
    this.createLangForm = {...createLangFormDefault};
  }

  @Mutation
  RESET_EDIT_LANG(): void {
    this.langForEdit = null; 
  }

  @Mutation
  SET_LOGO_FILES({formType, files}: {formType: FormType; files: Array<ImageFile>}): void {
    const data = this[formType];
    if(data !== null) {
      data.logoFiles = files;
    }
  }

  @Action({ commit : 'SET_LANGUAGE_LIST' })
  async getLanguages(): Promise<Array<LanguageItem>> {
    const result = await api.languages.getLanguages();
    return result.data.languages;
  }

  @Action
  async updateLanguage(): Promise<void> {
    if(this.langForEdit) {
      const payload = {
        id  : this.langForEdit.id,
        name: this.langForEdit.name,
        logo: ''
      };
  
      if(this.langForEdit.logoFiles.length > 0) {
        payload.logo = this.langForEdit.logoFiles[0].response;
      }

      await api.languages.updateLanguage(payload);
    }
  }

  @Action({rawError : true})
  async removeLanguage(langId: number): Promise<void> {
    await api.languages.removeLanguage(langId);
  }

  @Action({rawError : true})
  async createLanguage(): Promise<void> {
    const payload = {
      name: this.createLangForm.name,
      logo: ''
    };

    if(this.createLangForm.logoFiles.length > 0) {
      payload.logo = this.createLangForm.logoFiles[0].response;
    }

    await api.languages.createLanguage(payload);
  }
}