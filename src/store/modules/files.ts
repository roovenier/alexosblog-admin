import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { FileItem }                             from '~/api/files/types';
import api                                      from '~/utils/api';

export interface File extends FileItem {
  url: string;
}

@Module({
  name      : 'Files',
  namespaced: true
})
export default class Files extends VuexModule {
  fileList: Array<FileItem> = []

  get getFiles(): Array<File> {
    return this.fileList.map((file) => ({...file, url : this.context.rootState.apiURL + '/upload/' + file.filename}));
  }

  @Mutation
  SET_FILE_LIST(files: Array<FileItem>): void {
    this.fileList = files;
  }

  @Action({ commit : 'SET_FILE_LIST' })
  async getFileList(): Promise<Array<FileItem>> {
    const result = await api.files.getFiles();
    return result.data.files;
  }
}