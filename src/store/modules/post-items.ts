import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { PostItem }                             from '~/api/post-items/types';
import api                                      from '~/utils/api';

export interface PostItemCreate {
    title: string;
    description: string;
    text: string;
    lang: number | null;
    isActive: boolean;
}

export interface PostItemEdit {
  id: number;
  postId: number;
  title: string;
  description: string;
  text: string;
  lang: number;
  isActive: boolean;
}

const editPostItemDefault: PostItemEdit = {
  id         : 0,
  postId     : 0,
  title      : '',
  description: '',
  text       : '',
  lang       : 0,
  isActive   : true
};

const createPostItemDefault: PostItemCreate = {
  title      : '',
  description: '',
  text       : '',
  lang       : null,
  isActive   : true
};

@Module({
  name      : 'PostItems',
  namespaced: true
})
export default class PostItems extends VuexModule {
    postItemCreateForm: PostItemCreate = {...createPostItemDefault}
    postItemEditForm: PostItemEdit = {...editPostItemDefault}

    @Action({rawError : true})
    async createPostItem(): Promise<void> {
      if(this.postItemCreateForm.lang) {
        const payload = {
          postId     : this.context.rootState.Posts.postDetail.id,
          title      : this.postItemCreateForm.title,
          description: this.postItemCreateForm.description,
          text       : this.postItemCreateForm.text,
          langId     : this.postItemCreateForm.lang,
          isActive   : this.postItemCreateForm.isActive
        };
        
        await api.postItems.createPostItem(payload);
      }
    }

    @Action
    async removePostItem(postItemId: number): Promise<void> {
      await api.postItems.removePostItem(postItemId);
    }

    @Action
    async updatePostItem(): Promise<void> {
      const payload = {
        id         : this.postItemEditForm.id,
        title      : this.postItemEditForm.title,
        description: this.postItemEditForm.description,
        text       : this.postItemEditForm.text,
        langId     : this.postItemEditForm.lang,
        isActive   : this.postItemEditForm.isActive
      };

      await api.postItems.updatePostItem(payload);
    }

    @Mutation
    SET_POST_ITEM_EDIT(postItem: PostItem): void {
      this.postItemEditForm = {...postItem, lang : Number(postItem.lang.id)};
    }

    @Mutation
    RESET_CREATE_FORM(): void {
      this.postItemCreateForm = {...createPostItemDefault};
    }

    @Mutation
    RESET_EDIT_FORM(): void {
      this.postItemEditForm = {...editPostItemDefault};
    }
}