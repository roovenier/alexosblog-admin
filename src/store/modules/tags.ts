import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { TagItem, CreateTagPayload }            from '~/api/tags/types';
import api                                      from '~/utils/api';

const createFormDefault: CreateTagPayload = {
  name: ''
};

@Module({
  name      : 'Tags',
  namespaced: true
})
export default class Tags extends VuexModule {
    tagList: Array<TagItem> = []
    editDialogVisible = false
    tagForEdit: TagItem | null = null
    createTagForm: CreateTagPayload = {...createFormDefault}
    createDialogVisible = false

    @Mutation
    SET_TAG_LIST(list: Array<TagItem>): void {
      this.tagList = list;
    }

    @Mutation
    SET_EDIT_DIALOG_VISIBLE(value: boolean): void {
      this.editDialogVisible = value;
    }

    @Mutation
    SET_CREATE_DIALOG_VISIBLE(value: boolean): void {
      this.createDialogVisible = value;
    }

    @Mutation
    SET_TAG_FOR_EDIT(tag: TagItem): void {
      this.tagForEdit = {...tag};
    }

    @Mutation
    RESET_EDIT_TAG(): void {
      this.tagForEdit = null; 
    }

    @Mutation
    RESET_CREATE_TAG(): void {
      this.createTagForm = {...createFormDefault};
    }

    @Action({ commit : 'SET_TAG_LIST' })
    async getTags(): Promise<Array<TagItem>> {
      const result = await api.tags.getTags();
      return result.data.tags;
    }

    @Action
    async removeTag(tagId: number): Promise<void> {
      await api.tags.removeTag(tagId);
    }

    @Action
    async updateTag(): Promise<void> {
      if(this.tagForEdit) {
        const payload = {
          id  : this.tagForEdit.id,
          name: this.tagForEdit.name
        };
        await api.tags.updateTag(payload);
      }
    }

    @Action
    async createTag(): Promise<void> {
      await api.tags.createTag(this.createTagForm);
    }
}