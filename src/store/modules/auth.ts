import Vue                                      from 'vue';
import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators';
import jwtDecode                                from 'jwt-decode';
import { UserItem }                             from '~/api/users/types';
import api                                      from '~/utils/api';

export interface LoginData {
    email: string;
    password: string;
}

interface JWTTokenData extends UserItem {
  exp: number;
  iat: number;
}

@Module({
  name      : 'Auth',
  namespaced: true
})
export default class Auth extends VuexModule {
    loginData: LoginData = {
      email   : '',
      password: ''
    }
    user: JWTTokenData | null = null

    get getUser(): JWTTokenData | null {
      return this.user;
    }

    @Action
    async login(): Promise<void> {
      const result = await api.users.login(this.loginData);
      if(result.data && result.data.login.accessToken) {
        Vue.$cookies.set('token', result.data.login.accessToken);
      }
    }

    @Mutation
    CLEAR_USER_TOKEN(): void {
      Vue.$cookies.remove('token');
      this.user = null;
    }

    @Mutation
    SET_USER_FROM_TOKEN(): void {
      const token = Vue.$cookies.get('token');
      if(token) {
        const decoded: JWTTokenData = jwtDecode(token);
        if(decoded.exp < Date.now() / 1000) {
          Vue.$cookies.remove('token');
        } else {
          this.user = decoded;
        }
      }
    }
}