import Vue        from 'vue';
import Vuex       from 'vuex';

import Users      from '~/store/modules/users';
import Preloader  from '~/store/modules/preloader';
import Languages  from '~/store/modules/languages';
import Posts      from '~/store/modules/posts';
import PostItems  from '~/store/modules/post-items';
import Auth       from '~/store/modules/auth';
import Tags       from '~/store/modules/tags';
import Files      from '~/store/modules/files';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    apiURL: process.env.VUE_APP_API_URL
  },
  modules: {
    Users,
    Preloader,
    Languages,
    Posts,
    PostItems,
    Auth,
    Tags,
    Files
  }
});

export default store;