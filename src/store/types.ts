export interface ImageFile {
    name: string;
    response: string;
    url: string;
}