import Vue                        from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import store                      from '~/store/index';
import Home                       from '~/views/Home.vue';
import Page404                    from '~/views/404.vue';
import FileBrowser                from '~/views/FileBrowser.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path     : '/404',
    name     : 'Page 404',
    component: Page404,
    meta     : {
      layout      : 'default-layout',
      requiresAuth: true
    },
  },
  {
    path     : '/file-browser',
    name     : 'File browser',
    component: FileBrowser,
    meta     : {
      layout      : 'auth-layout',
      requiresAuth: true
    },
  },
  {
    path     : '/',
    name     : 'Home',
    component: Home,
    meta     : {
      layout      : 'default-layout',
      requiresAuth: true
    },
  },
  {
    path: '/users',
    name: 'Users',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/users/Users.vue'),
    beforeEnter: (to, from, next): void => {
      store.commit('Preloader/SET_PRELOADER_STATE', true, {root : true});
      next();
    }
  },
  {
    path: '/langs',
    name: 'Languages',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/languages/Langs.vue'),
    beforeEnter: (to, from, next): void => {
      store.commit('Preloader/SET_PRELOADER_STATE', true, {root : true});
      next();
    }
  },
  {
    path: '/langs/new',
    name: 'New language',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component: (): Promise<typeof import('*.vue')> => import('~/views/languages/NewLang.vue')
  },
  {
    path: '/posts',
    name: 'Posts',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/posts/Posts.vue'),
    beforeEnter: (to, from, next): void => {
      store.commit('Preloader/SET_PRELOADER_STATE', true, {root : true});
      next();
    }
  },
  {
    path: '/posts/new',
    name: 'New post',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component: (): Promise<typeof import('*.vue')> => import('~/views/posts/NewPost.vue')
  },
  {
    path: '/posts/:id',
    name: 'Post Detail',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/posts/PostDetail.vue'),
    beforeEnter: (to, from, next): void => {
      store.commit('Preloader/SET_PRELOADER_STATE', true, {root : true});
      next();
    }
  },
  {
    path: '/posts/:id/post-item/new',
    name: 'New Post Item',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/posts/NewPostItem.vue'),
    beforeEnter: (to, from, next): void => {
      store.commit('Preloader/SET_PRELOADER_STATE', true, {root : true});
      next();
    }
  },
  {
    path: '/posts/:id/post-item/:itemId',
    name: 'Post Item Detail',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/posts/PostItemDetail.vue'),
    beforeEnter: (to, from, next): void => {
      store.commit('Preloader/SET_PRELOADER_STATE', true, {root : true});
      next();
    }
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      layout: 'auth-layout'
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/Login.vue'),
    beforeEnter: (to, from, next): void => {
      if(store.getters['Auth/getUser']) {
        return next({
          path: '/'
        });
      }
      return next();
    }
  },
  {
    path: '/tags',
    name: 'Tags',
    meta: {
      layout      : 'default-layout',
      requiresAuth: true
    },
    component  : (): Promise<typeof import('*.vue')> => import('~/views/Tags.vue'),
    beforeEnter: (to, from, next): void => {
      store.commit('Preloader/SET_PRELOADER_STATE', true, {root : true});
      next();
    }
  },
];

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
});

router.beforeEach((to, from, next) => {
  store.commit('Auth/SET_USER_FROM_TOKEN', null, {root : true});
  
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters['Auth/getUser']) {
      return next({
        path: '/login',
      });
    } else {
      return next();
    }
  } else {
    return next();
  }
});

export default router;
