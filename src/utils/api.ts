import Vue                                      from 'vue';
import ApolloClient                             from 'apollo-boost';
import { Operation }                            from 'apollo-link';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import Api                                      from '~/api/index';
import router                                   from '~/router';

const apolloClient = new ApolloClient<NormalizedCacheObject>({
  uri    : `${process.env.VUE_APP_API_URL}/graphql`,
  cache  : new InMemoryCache(),
  request: (operation: Operation): void => {
    const token = Vue.$cookies.get('token');
    operation.setContext({
      headers: {
        authorization: token ? `Bearer ${token}` : ''
      }
    });
  },
  onError(error): void {
    if(error.graphQLErrors) {
      const err = error.graphQLErrors[0];
      if(err.message === 'Unauthorized') {
        Vue.$cookies.remove('token');
        if(router.currentRoute.name !== 'Login') {
          window.location.reload();
        }
      }
    }
  }
});

export default Api(apolloClient);