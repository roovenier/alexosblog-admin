import ApolloClient              from 'apollo-boost';
import { FetchResult }           from 'apollo-link';
import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import { ApiUsers }              from '~/api/users/types';
import { ApiLanguages }          from '~/api/languages/types';
import { ApiPosts }              from '~/api/posts/types';
import { ApiPostItems }          from '~/api/post-items/types';
import { ApiTags }               from '~/api/tags/types';
import { ApiFiles }              from '~/api/files/types';

export type NullResponseFetchType = Promise<FetchResult<null>>

export type ApolloClientType = ApolloClient<NormalizedCacheObject>;

export interface ApiModule {
    users: ApiUsers;
    languages: ApiLanguages;
    posts: ApiPosts;
    postItems: ApiPostItems;
    tags: ApiTags;
    files: ApiFiles;
}