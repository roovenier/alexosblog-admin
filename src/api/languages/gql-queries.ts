import gql from 'graphql-tag';

export const GQL_GET_ALL_LANGAUGES = gql`
    query {
        languages {
            id
            name
            logo
        }
    }
`;

export const GQL_UPDATE_LANGUAGE = gql`
    mutation ($language: UpdateLanguageDto!) {
        updateLanguage(language: $language) {
            id
            name
            logo
        }
    }
`;

export const GQL_REMOVE_LANGUAGE = gql`
    mutation ($id: Int!) {
        removeLanguage(id: $id)
    }
`;

export const GQL_CREATE_LANGUAGE = gql`
    mutation ($language: CreateLanguageDto!) {
        createLanguage(language: $language) {
            id
            name
            logo
        }
    }
`;