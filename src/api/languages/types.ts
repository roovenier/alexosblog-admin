import { ApolloQueryResult }     from 'apollo-client';
import { FetchResult }           from 'apollo-link';
import { NullResponseFetchType } from '~/api/types';

export interface LanguageItem {
    id: number;
    name: string;
    logo: string | null;
}

export interface CreateLanguagePayload {
    name: string;
    logo: string;
}

export interface CreateLanguageResponse {
    createLanguage: LanguageItem;
}

export type CreateLanguageResponseType = Promise<FetchResult<CreateLanguageResponse>>

export interface GetLanguagesResponse {
    languages: Array<LanguageItem>;
}
export type GetLanguagesResponseType = Promise<ApolloQueryResult<GetLanguagesResponse>>

export interface ApiLanguages {
    getLanguages: () => GetLanguagesResponseType;
    updateLanguage: (payload: LanguageItem) => NullResponseFetchType;
    removeLanguage: (langId: number) => NullResponseFetchType;
    createLanguage: (payload: CreateLanguagePayload) => CreateLanguageResponseType;
}