import { NullResponseFetchType, ApolloClientType } from '~/api/types';
import { 
  ApiLanguages,
  GetLanguagesResponseType,
  CreateLanguageResponseType,
  LanguageItem,
  GetLanguagesResponse } from '~/api/languages/types';
import { GQL_GET_ALL_LANGAUGES,
  GQL_UPDATE_LANGUAGE,
  GQL_REMOVE_LANGUAGE,
  GQL_CREATE_LANGUAGE } from '~/api/languages/gql-queries';

const methods = (apolloClient: ApolloClientType): ApiLanguages => ({
  getLanguages(): GetLanguagesResponseType {
    return apolloClient.query({
      query    : GQL_GET_ALL_LANGAUGES,
      variables: {}
    });
  },
  updateLanguage(payload: LanguageItem): NullResponseFetchType {
    return apolloClient.mutate({
      mutation : GQL_UPDATE_LANGUAGE,
      variables: {
        language: payload
      }
    });
  },
  removeLanguage(langId: number): NullResponseFetchType {
    return apolloClient.mutate({
      mutation : GQL_REMOVE_LANGUAGE,
      variables: {
        id: langId
      },
      update(cache) {
        const cacheData: GetLanguagesResponse | null  = cache.readQuery({ query : GQL_GET_ALL_LANGAUGES });
        if(cacheData) {
          cacheData.languages = cacheData.languages.filter((language) => language.id !== langId);
          cache.writeQuery({ query : GQL_GET_ALL_LANGAUGES, data : cacheData });
        }
      }
    });
  },
  createLanguage(payload): CreateLanguageResponseType {
    return apolloClient.mutate({
      mutation : GQL_CREATE_LANGUAGE,
      variables: {
        language: payload
      },
      update(cache, result) {
        if(result.data) {
          const { createLanguage } = result.data;
    
          const cacheData: GetLanguagesResponse | null = cache.readQuery({ query : GQL_GET_ALL_LANGAUGES });
          if(cacheData) {
            cacheData.languages = [...cacheData.languages, createLanguage];
            cache.writeQuery({ query : GQL_GET_ALL_LANGAUGES, data : cacheData });
          }
        }
      },
    });
  }
});

export default methods;