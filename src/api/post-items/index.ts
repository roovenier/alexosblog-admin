import { ApolloClientType, NullResponseFetchType }                          from '~/api/types';
import { ApiPostItems, CreatePostItemType, RemovePostItemType }             from '~/api/post-items/types';
import { GQL_CREATE_POST_ITEM, GQL_REMOVE_POST_ITEM, GQL_UPDATE_POST_ITEM } from '~/api/post-items/gql-queries';
import { GQL_GET_POST_BY_ID }                                               from '~/api/posts/gql-queries';
import { GetPostByIdResponse }                                              from '~/api/posts/types';

const methods = (apolloClient: ApolloClientType): ApiPostItems => ({
  createPostItem(payload): CreatePostItemType {
    return apolloClient.mutate({
      mutation : GQL_CREATE_POST_ITEM,
      variables: {
        postItem: payload
      },
      update(cache, result) {
        if(result.data) {
          const { createPostItem } = result.data;
      
          const cacheData: GetPostByIdResponse | null = cache.readQuery({
            query    : GQL_GET_POST_BY_ID,
            variables: {
              postId: payload.postId
            }
          });
          if(cacheData) {
            cacheData.postByIdAdmin.postItems = [...cacheData.postByIdAdmin.postItems, createPostItem];
            cache.writeQuery({
              query    : GQL_GET_POST_BY_ID,
              variables: {
                postId: payload.postId
              },
              data: cacheData
            });
          }
        }
      }
    });
  },
  removePostItem(postItemId): RemovePostItemType {
    return apolloClient.mutate({
      mutation : GQL_REMOVE_POST_ITEM,
      variables: {
        id: postItemId
      },
      update(cache, result) {
        if(result.data) {
          const { removePostItem } = result.data;
  
          const cacheData: GetPostByIdResponse | null  = cache.readQuery({ 
            query    : GQL_GET_POST_BY_ID,
            variables: {
              postId: removePostItem.postId
            }
          });
          if(cacheData) {
            cacheData.postByIdAdmin.postItems = cacheData.postByIdAdmin.postItems.filter((postItem) => postItem.id !== postItemId);
            cache.writeQuery({
              query    : GQL_GET_POST_BY_ID,
              data     : cacheData,
              variables: {
                postId: removePostItem.postId
              }
            });
          }
        }
      }
    });
  },
  updatePostItem(payload): NullResponseFetchType {
    return apolloClient.mutate({
      mutation : GQL_UPDATE_POST_ITEM,
      variables: {
        postItem: payload
      }
    });
  }
});

export default methods;