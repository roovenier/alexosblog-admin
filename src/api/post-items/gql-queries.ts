import gql from 'graphql-tag';

export const GQL_CREATE_POST_ITEM = gql`
    mutation ($postItem: CreatePostItemDto!) {
        createPostItem(postItem: $postItem) {
            id
            title
            description
            text
            isActive
            lang {
                id
                name
                logo
            }
        }
    }
`;

export const GQL_REMOVE_POST_ITEM = gql`
    mutation ($id: Int!) {
        removePostItem(id: $id) {
            postId
            title
        }
    }
`;

export const GQL_UPDATE_POST_ITEM = gql`
    mutation ($postItem: UpdatePostItemDto!) {
        updatePostItem(postItem: $postItem) {
            id
            title
            description
            text
            isActive
            lang {
                id
                name
                logo
            }
        }
    }
`;