import { FetchResult }           from 'apollo-link';
import { NullResponseFetchType } from '~/api/types';
import { LanguageItem }          from '~/api/languages/types';

export interface PostItem {
    id: number;
    postId: number;
    title: string;
    description: string;
    text: string;
    lang: LanguageItem;
    isActive: boolean;
}

export interface CreatePostItemPayload {
    postId: number;
    title: string;
    description: string;
    text: string;
    langId: number;
    isActive: boolean;
}

export interface EditPostItemPayload {
    id: number;
    title: string;
    description: string;
    text: string;
    langId: number;
    isActive: boolean;
}

export interface CreatePostItemResponse {
    createPostItem: PostItem;
}

export type CreatePostItemType = Promise<FetchResult<CreatePostItemResponse>>

export interface RemovePostItemResponse {
    removePostItem: {
        postId: number;
        title: string;
    };
}

export type RemovePostItemResolvedType = FetchResult<RemovePostItemResponse>
export type RemovePostItemType = Promise<RemovePostItemResolvedType>

export interface ApiPostItems {
    createPostItem: (payload: CreatePostItemPayload) => CreatePostItemType;
    removePostItem: (postItemId: number) => RemovePostItemType;
    updatePostItem: (payload: EditPostItemPayload) => NullResponseFetchType;
}