import { ApolloQueryResult }     from 'apollo-client';
import { FetchResult }           from 'apollo-link';
import { NullResponseFetchType } from '~/api/types';

export interface UserItem {
    id: number;
    isActive: boolean;
    email: string;
    firstName: string;
    lastName: string;
}

export interface GetUsersResponse {
    users: Array<UserItem>;
}
export type GetUsersResponseType = Promise<ApolloQueryResult<GetUsersResponse>>

export interface LoginPayload {
    email: string;
    password: string;
}

export interface LoginResponse {
    login: {
        accessToken: string;
    };
}
export type LoginResponseType = Promise<FetchResult<LoginResponse>>

export interface ApiUsers {
    getUsers: () => GetUsersResponseType;
    updateUser: (payload: UserItem) => NullResponseFetchType;
    login: (payload: LoginPayload) => LoginResponseType;
}