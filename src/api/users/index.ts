import { ApolloClientType, NullResponseFetchType }                     from '~/api/types';
import { ApiUsers, GetUsersResponseType, UserItem, LoginResponseType } from '~/api/users/types';
import { GQL_GET_USERS, GQL_UPDATE_USER, GQL_LOGIN }                   from '~/api/users/gql-queries';

const methods = (apolloClient: ApolloClientType): ApiUsers => ({
  getUsers(): GetUsersResponseType {
    return apolloClient.query({
      query    : GQL_GET_USERS,
      variables: {}
    });
  },
  updateUser(payload: UserItem): NullResponseFetchType {
    return apolloClient.mutate({
      mutation : GQL_UPDATE_USER,
      variables: {
        user: payload
      }
    });
  },
  login(payload): LoginResponseType {
    return apolloClient.mutate({
      mutation : GQL_LOGIN,
      variables: {
        user: payload
      }
    });
  }
});

export default methods;