import gql from 'graphql-tag';

export const GQL_GET_USERS = gql`
    query {
        users {
            id
            isActive
            email
            firstName
            lastName
        }
    }
`;

export const GQL_UPDATE_USER = gql`
    mutation ($user: UpdateUserDto!) {
        updateUser(user: $user) {
            id
            isActive
            email
            firstName
            lastName
        }
    }
`;

export const GQL_LOGIN = gql`
    mutation ($user: LoginUserDto!) {
        login(user: $user) {
            accessToken
        }
    }
`;