import { ApiModule, ApolloClientType } from '~/api/types';
import usersApi                        from '~/api/users/index';
import languagesApi                    from '~/api/languages/index';
import postsApi                        from '~/api/posts/index';
import postItemsApi                    from '~/api/post-items/index';
import tagsApi                         from '~/api/tags/index';
import filesApi                        from '~/api/files/index';

const Api = (apolloClient: ApolloClientType): ApiModule => ({
  users    : usersApi(apolloClient),
  languages: languagesApi(apolloClient),
  posts    : postsApi(apolloClient),
  postItems: postItemsApi(apolloClient),
  tags     : tagsApi(apolloClient),
  files    : filesApi(apolloClient)
});

export default Api;