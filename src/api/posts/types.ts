import { ApolloQueryResult } from 'apollo-client';
import { FetchResult }       from 'apollo-link';
import { PostItem }          from '~/api/post-items/types';
import { TagItem }           from '~/api/tags/types';

export interface Post {
    id: number;
    userId: number;
    title: string;
    postItems: Array<PostItem>;
    tags: Array<TagItem> | Array<number>;
}

export interface CreatePostPayload {
    title: string;
    tagIds: Array<number>;
}

export interface UpdatePostPayload {
    id: number;
    title: string;
    tagIds: Array<number>;
}

export interface GetPostsResponse {
    postsByUser: Array<Post>;
}
export type GetPostsResponseType = Promise<ApolloQueryResult<GetPostsResponse>>

export interface GetPostByIdResponse {
    postByIdAdmin: Post;
}
export type GetPostByIdResponseType = Promise<ApolloQueryResult<GetPostByIdResponse>>

export interface CreatePostResponse {
    createPost: Post;
}

export type CreatePostResponseType = Promise<FetchResult<CreatePostResponse>>

export interface RemovePostResponse {
    removePost: {
        userId: number;
        title: string;
    };
}

export type RemovePostResponseType = Promise<FetchResult<RemovePostResponse>>

export interface UpdatePostResponse {
    updatePost: Post;
}

export type UpdatePostResponseType = Promise<FetchResult<UpdatePostResponse>>

export interface ApiPosts {
    getUserPosts: (userId: number) => GetPostsResponseType;
    getPostById: (postId: number) => GetPostByIdResponseType;
    createPost: (payload: CreatePostPayload) => CreatePostResponseType;
    updatePost: (payload: UpdatePostPayload) => UpdatePostResponseType;
    removePost: (postId: number) => RemovePostResponseType;
}