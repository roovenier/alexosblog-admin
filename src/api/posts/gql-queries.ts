import gql from 'graphql-tag';

export const GQL_GET_USER_POSTS = gql`
    query ($userId: Int!) {
        postsByUser(userId: $userId) {
            id
            userId
            title
            tags {
                id
                name
            }
            createdAt
            postItems {
                id
                title
                description
                text
                isActive
                lang {
                    id
                    name
                    logo
                }
            }
        }
    }
`;

export const GQL_GET_POST_BY_ID = gql`
    query ($postId: Int!) {
        postByIdAdmin(postId: $postId) {
            id
            userId
            title
            tags {
                id
                name
            }
            postItems {
                id
                title
                description
                text
                isActive
                lang {
                    id
                    name
                    logo
                }
            }
        }
    }
`;

export const GQL_CREATE_POST = gql`
    mutation ($post: CreatePostDto!) {
        createPost(post: $post) {
            id
            title
            userId
        }
    }
`;

export const GQL_UPDATE_POST = gql`
    mutation ($post: UpdatePostDto!) {
        updatePost(post: $post) {
            id
            userId
            title
            tags {
                id
                name
            }
            createdAt
            postItems {
                id
                title
                description
                text
                isActive
                lang {
                    id
                    name
                    logo
                }
            }
        }
    }
`;

export const GQL_REMOVE_POST = gql`
    mutation ($id: Int!) {
        removePost(id: $id) {
            userId
            title
        }
    }
`;