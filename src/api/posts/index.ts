import { ApolloClientType } from '~/api/types';
import { 
  ApiPosts,
  GetPostsResponseType,
  GetPostByIdResponseType,
  CreatePostResponseType,
  GetPostsResponse,
  RemovePostResponseType,
  UpdatePostResponseType } from '~/api/posts/types';
import { GQL_GET_USER_POSTS, GQL_GET_POST_BY_ID, GQL_CREATE_POST, GQL_UPDATE_POST, GQL_REMOVE_POST }                                                                                                                                                                                                                                                                                              from '~/api/posts/gql-queries';

const methods = (apolloClient: ApolloClientType): ApiPosts => ({
  getUserPosts(userId): GetPostsResponseType {
    return apolloClient.query({
      query    : GQL_GET_USER_POSTS,
      variables: {
        userId
      }
    });
  },
  getPostById(postId): GetPostByIdResponseType {
    return apolloClient.query({
      query    : GQL_GET_POST_BY_ID,
      variables: {
        postId
      }
    });
  },
  createPost(payload): CreatePostResponseType {
    return apolloClient.mutate({
      mutation : GQL_CREATE_POST,
      variables: {
        post: payload
      },
      update(cache, result) {
        if(result.data) {
          const { createPost } = result.data;
    
          const cacheData: GetPostsResponse | null = cache.readQuery({
            query    : GQL_GET_USER_POSTS,
            variables: {
              userId: createPost.userId
            }
          });
          if(cacheData) {
            cacheData.postsByUser = [createPost, ...cacheData.postsByUser];
            cache.writeQuery({
              query    : GQL_GET_USER_POSTS,
              variables: {
                userId: createPost.userId
              },
              data: cacheData
            });
          }
        }
      }
    });
  },
  updatePost(payload): UpdatePostResponseType {
    return apolloClient.mutate({
      mutation : GQL_UPDATE_POST,
      variables: {
        post: payload
      }
    });
  },
  removePost(postId): RemovePostResponseType {
    return apolloClient.mutate({
      mutation : GQL_REMOVE_POST,
      variables: {
        id: postId
      },
      update(cache, result) {
        if(result.data) {
          const { removePost } = result.data;

          const cacheData: GetPostsResponse | null  = cache.readQuery({ 
            query    : GQL_GET_USER_POSTS,
            variables: {
              userId: removePost.userId
            }
          });
          if(cacheData) {
            cacheData.postsByUser = cacheData.postsByUser.filter((post) => post.id !== postId);
            cache.writeQuery({
              query    : GQL_GET_USER_POSTS,
              data     : cacheData,
              variables: {
                userId: removePost.userId
              }
            });
          }
        }
      }
    });
  }
});

export default methods;