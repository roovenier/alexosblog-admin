import { ApolloQueryResult }     from 'apollo-client';
import { FetchResult }           from 'apollo-link';
import { NullResponseFetchType } from '~/api/types';

export interface TagItem {
    id: number;
    name: string;
}

export interface CreateTagPayload {
    name: string;
}

export interface GetTagsResponse {
    tags: Array<TagItem>;
}
export type GetTagsResponseType = Promise<ApolloQueryResult<GetTagsResponse>>

export interface CreateTagResponse {
    createTag: TagItem;
}

export type CreateTagResponseType = Promise<FetchResult<CreateTagResponse>>

export interface ApiTags {
    getTags: () => GetTagsResponseType;
    removeTag: (tagId: number) => NullResponseFetchType;
    updateTag: (payload: TagItem) => NullResponseFetchType;
    createTag: (payload: CreateTagPayload) => CreateTagResponseType;
}