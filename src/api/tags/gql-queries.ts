import gql from 'graphql-tag';

export const GQL_GET_TAGS = gql`
    query {
        tags {
            id
            name
        }
    }
`;

export const GQL_REMOVE_TAG = gql`
    mutation ($id: Int!) {
        removeTag(id: $id)
    }
`;

export const GQL_UPDATE_TAG = gql`
    mutation ($tag: UpdateTagDto!) {
        updateTag(tag: $tag) {
            id
            name
        }
    }
`;

export const GQL_CREATE_TAG = gql`
    mutation ($tag: CreateTagDto!) {
        createTag(tag: $tag) {
            id
            name
        }
    }
`;