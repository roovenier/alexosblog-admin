import { ApolloClientType, NullResponseFetchType }                              from '~/api/types';
import { ApiTags, GetTagsResponse, GetTagsResponseType, CreateTagResponseType } from '~/api/tags/types';
import { GQL_GET_TAGS, GQL_REMOVE_TAG, GQL_UPDATE_TAG, GQL_CREATE_TAG }         from '~/api/tags/gql-queries';

const methods = (apolloClient: ApolloClientType): ApiTags => ({
  getTags(): GetTagsResponseType {
    return apolloClient.query({
      query    : GQL_GET_TAGS,
      variables: {}
    });
  },
  removeTag(tagId: number): NullResponseFetchType {
    return apolloClient.mutate({
      mutation : GQL_REMOVE_TAG,
      variables: {
        id: tagId
      },
      update(cache) {
        const cacheData: GetTagsResponse | null  = cache.readQuery({ query : GQL_GET_TAGS });
        if(cacheData) {
          cacheData.tags = cacheData.tags.filter((tag) => tag.id !== tagId);
          cache.writeQuery({ query : GQL_GET_TAGS, data : cacheData });
        }
      }
    });
  },
  updateTag(payload): NullResponseFetchType {
    return apolloClient.mutate({
      mutation : GQL_UPDATE_TAG,
      variables: {
        tag: payload
      }
    });
  },
  createTag(payload): CreateTagResponseType {
    return apolloClient.mutate({
      mutation : GQL_CREATE_TAG,
      variables: {
        tag: payload
      },
      update(cache, result) {
        if(result.data) {
          const { createTag } = result.data;
    
          const cacheData: GetTagsResponse | null = cache.readQuery({
            query: GQL_GET_TAGS
          });
          if(cacheData) {
            cacheData.tags = [...cacheData.tags, createTag];
            cache.writeQuery({
              query: GQL_GET_TAGS,
              data : cacheData
            });
          }
        }
      }
    });
  }
});

export default methods;