import { ApolloQueryResult } from 'apollo-client';

export interface FileItem {
    filename: string;
}

export interface GetFilesResponse {
    files: Array<FileItem>;
}
export type GetFilesResponseType = Promise<ApolloQueryResult<GetFilesResponse>>

export interface ApiFiles {
    getFiles: () => GetFilesResponseType;
}