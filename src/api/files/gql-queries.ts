import gql from 'graphql-tag';

export const GQL_GET_FILES = gql`
    query {
        files {
            filename
        }
    }
`;