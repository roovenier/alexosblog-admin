import { ApolloClientType }               from '~/api/types';
import { ApiFiles, GetFilesResponseType } from '~/api/files/types';
import { GQL_GET_FILES }                  from '~/api/files/gql-queries';

const methods = (apolloClient: ApolloClientType): ApiFiles => ({
  getFiles(): GetFilesResponseType {
    return apolloClient.query({
      query    : GQL_GET_FILES,
      variables: {}
    });
  }
});

export default methods;