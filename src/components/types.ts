export interface ValidationRules {
    [key: string]: Array<{
        required?: boolean;
        message?: string;
        trigger?: string;
    }>;
}