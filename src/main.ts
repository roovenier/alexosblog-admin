import Vue, { CreateElement, VNode } from 'vue';
import App                           from '~/App.vue';
import router                        from '~/router/index';
import store                         from '~/store';
import Default                       from '~/layouts/Default.vue';
import Auth                          from '~/layouts/Auth.vue';
import '~/assets/styles/common.scss';

import Element                  from 'element-ui';
import locale                   from 'element-ui/lib/locale/lang/en';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(Element, { locale });

import VueCookies from 'vue-cookies';
Vue.use(VueCookies);

import CKEditor from 'ckeditor4-vue';
Vue.use(CKEditor);

Vue.component('default-layout', Default);
Vue.component('auth-layout', Auth);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h: CreateElement): VNode => h(App)
}).$mount('#app');
